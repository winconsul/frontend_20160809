Vue = require "vue"

new Vue
    el: "#main"
    data: () ->
        todos: [
            { id: 1, title: "買い物", finished: false, details: [ "にんじん", "じゃがいも", ] }
            { id: 2, title: "勉強",   finished: false, details: [] }
        ]
    methods:
        add: () ->
            id = 1 + Math.max.apply null, (todo.id for todo in @todos)
            @todos.push { id: id, title: "", finished: false, details: [] }
        remove: (id) ->
            for todo, index in @todos
                if todo.id == id
                    @todos.splice index, 1
                    break
            return
 